#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mpi.h>

#include "md5.h"

#define MAX_NUM_TASKS 65536
#define NUM_BLANK_SPACES 2
#define RESULT_MAX_SIZE MD5_SIZE+NUM_BLANK_SPACES+FILENAME_MAX

#ifndef DEBUG
  #define DEBUG  0
#endif 

#ifndef DEBUG2
  #define DEBUG2 0
#endif 

#ifndef DEBUG3
  #define DEBUG3 0
#endif 

typedef char *unit_result_t;
typedef char *unit_of_work_t;

/* Local functions master/slave*/
static int master(const char *filename, unit_of_work_t * list_of_work);
static void slave(void);
static unit_of_work_t get_work_item( unit_of_work_t *list_of_work, int submittedtask);
static void process_results(unit_result_t result);
static unit_result_t do_work(unit_of_work_t work);

static void addFinishedJob(unit_result_t job,int pos);
static void inicializeGobalVars();

/* Local functions md5sumlist*/
static void usage(void);
static void print_sig(const unsigned char *sig);
static char *read_file(const char *filename);
static void read_list(const char *filename, unit_of_work_t *list_of_work, int *ntasks);

int max_num_tasks = MAX_NUM_TASKS;
int dieTag = MAX_NUM_TASKS+1;
char * listFinishedJobs;

/*
 * Main
 */
int main(int argc, char **argv)
{
    int myrank, numprocs;
    char *infile = NULL;
    int ntasks = 0;
    double maxtime,mintime,avgtime,mytime;
    unit_of_work_t * list_of_work;
    unit_of_work_t work;

    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

    if(myrank==0){
      inicializeGobalVars();
    }

    if ((numprocs < 2)||(argc > 2)) {
      if (myrank ==0) {
      	usage();
      }
      MPI_Finalize();
      return -1;
    }
    
    if (argc == 1) {
      infile==NULL;
    }
    else if (argc == 2) {
      infile = argv[1];
    }

    MPI_Barrier(MPI_COMM_WORLD); /*synchronize all processes*/
    mytime = MPI_Wtime(); /*get time just before work section */

    if (myrank == 0) {

    list_of_work = (unit_of_work_t *)malloc(sizeof(unit_of_work_t *)*max_num_tasks);

    ntasks = master(infile,list_of_work);

    } else {
      slave();
    }

    mytime = MPI_Wtime() - mytime; /*get time just after work section*/

    /*compute max, min, and average timing statistics*/
    MPI_Reduce(&mytime, &maxtime, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    MPI_Reduce(&mytime, &mintime, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
    MPI_Reduce(&mytime, &avgtime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    if (!myrank) {
        /* Print results */
        int i = 0,leng=0;
        for(i=0;i<ntasks;i++){
          print_sig(listFinishedJobs+leng);
          work = get_work_item(list_of_work,i);
          printf("  %s\n",work);
          leng += MD5_SIZE+1;
        }

      avgtime /= numprocs;
      printf("Min: %lf Max: %lf Avg: %lf\n", mintime, maxtime,avgtime);
    }


    MPI_Finalize();
    return 0;
}

/*
 * print the usage message
 */
static	void	usage(void)
{
  fprintf(stderr, " Usage: mpirun -n $NP md5sumlist [filelist]\n");
  fprintf(stderr, "        (where $NP > 1)\n");    
}


/*
 * Master
 */
static int master(const char *filename, unit_of_work_t * list_of_work)
{
  int numprocs, total_procs, rank;
  unit_of_work_t work;
  unit_result_t result;
  int ntasks = 0, submittedtasks = 0;
  int next_message_number = 0;
  MPI_Status status;

  result = (unit_result_t) malloc (sizeof(char)*RESULT_MAX_SIZE);

  read_list(filename, list_of_work, &ntasks);

  /* Find out how many processes there are in the default communicator */
  MPI_Comm_size(MPI_COMM_WORLD, &total_procs);

  /* Seed the slaves; send one unit of work to each slave. */
  if (ntasks<total_procs) {
    numprocs = ntasks+1;
  }else{
    numprocs = total_procs;
  }

  for (rank = 1; rank < numprocs; ++rank) {

    /* Find the next item of work to do */
    work = get_work_item(list_of_work, submittedtasks);
    submittedtasks++;

    if (DEBUG) {
       printf("\n  [MASTER sending] work=%s length=%d to SLAVE #%d \n",work,strlen(work), rank);
    }

    /* Send it to each rank */
    MPI_Send(work,             /* message buffer */
             strlen(work),     /* one data item */
             MPI_CHAR,         /* data item is an integer */
             rank,             /* destination process rank */
             next_message_number++,          /* user chosen message tag */
             MPI_COMM_WORLD);  /* default communicator */
  }

  /* Loop over getting new work requests until there is no more work to be done */

  work = get_work_item(list_of_work, submittedtasks);
  submittedtasks++;

  while (work != NULL) {

    /* Receive results from a slave */
    memset(result,'\0',RESULT_MAX_SIZE);
    MPI_Recv(result,           /* message buffer */
             RESULT_MAX_SIZE,  /* one data item */
             MPI_CHAR,         /* of type double real */
             MPI_ANY_SOURCE,   /* receive from any sender */
             MPI_ANY_TAG,      /* any type of message */
             MPI_COMM_WORLD,   /* default communicator */
             &status);         /* info about the received message */

    if (DEBUG) {
       int count;
       MPI_Get_count(&status, MPI_CHAR, &count);
       printf("\n  [MASTER collecting] result with length=%d and count=%d from SLAVE #%d\n",
		   strlen(result), count, status.MPI_SOURCE);
    }

    addFinishedJob(result,status.MPI_TAG);

    if (DEBUG) {
       printf("\n  [MASTER sending] work=%s length=%d to SLAVE #%d\n", 
		   work, strlen(work), status.MPI_SOURCE);
    }

    /* Send the slave a new work unit */
    MPI_Send(work,             /* message buffer */
             strlen(work),     /* one data item */
             MPI_CHAR,         /* data item is an integer */
             status.MPI_SOURCE,/* to who we just received from */
             next_message_number++,          /* user chosen message tag */
             MPI_COMM_WORLD);  /* default communicator */

    /* Get the next unit of work to be done */
    work = get_work_item(list_of_work, submittedtasks);
    submittedtasks++;
  }

  /* There's no more work to be done, so receive all the outstanding
     results from the slaves. */
  for (rank = 1; rank < numprocs; ++rank) {

    memset(result,'\0',RESULT_MAX_SIZE);
    MPI_Recv(result,
              RESULT_MAX_SIZE,
              MPI_CHAR,
              MPI_ANY_SOURCE,
              MPI_ANY_TAG,
              MPI_COMM_WORLD,
              &status);

    if (DEBUG) {
        int count;
        MPI_Get_count(&status, MPI_CHAR, &count);
        printf("\n  [MASTER collecting END] result with length=%d and count=%d from SLAVE #%d\n",
          strlen(result), count, status.MPI_SOURCE);
        process_results(result);
    }
   
    addFinishedJob(result,status.MPI_TAG);
  }

  /* Tell all the slaves to exit by sending an empty message with DIETAG. */
  for (rank = 1; rank < total_procs; ++rank) {
    MPI_Send(0, 0, MPI_INT, rank, dieTag, MPI_COMM_WORLD);
  }

  return submittedtasks-1; 
}


/*
 * Slave
 */
static void slave(void)
{
  unit_of_work_t work;
  unit_result_t result;
  int myrank;
  MPI_Status status;

  work = (unit_of_work_t) malloc (sizeof(char)*FILENAME_MAX);
  result = (unit_result_t) malloc (sizeof(char)*RESULT_MAX_SIZE);

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

  while (1) {

    memset(work,'\0',FILENAME_MAX);

    /* Receive a message from the master */
    MPI_Recv(work,
              FILENAME_MAX,
              MPI_CHAR,
              0,
              MPI_ANY_TAG,
              MPI_COMM_WORLD,
              &status);

    int tag = status.MPI_TAG;

    /* Check the tag of the received message. */
    if (tag == dieTag) {
      return;
    }

    if (DEBUG) {
       printf("\n  [SLAVE #%d receiving] work=%s length=%d",myrank,work,strlen(work));
    }

    /* Do the work */
    result = do_work(work);

    if (DEBUG3) {
       printf("\n  [SLAVE #%d replaying] with result with length=%d",myrank,strlen(result));
       printf("\n  [SLAVE #%d replaying] Result=",myrank);
       process_results(result);
    }

    /* Send the result back */
    MPI_Send(result,
              MD5_SIZE+strlen(result+MD5_SIZE),
              MPI_CHAR,
              0,
              tag,
              MPI_COMM_WORLD);
  }
  free(result);
  free(work);
}

/*
 * Print an md5 signature
 */
static	void	print_sig(const unsigned char *sig)
{
  const unsigned char	*sig_p;
  
  for (sig_p = sig; sig_p < sig + MD5_SIZE; sig_p++) {
    printf("%02x", *sig_p);
  }
}

/*
 * Read in from stdin/file and run MD5 on the input
 */
static	char 	*read_file(const char *filename)
{
  unsigned char	sig[MD5_SIZE];
  char		buffer[4096];
  md5_t		md5;
  int		ret;
  FILE		*stream;
  
  if (filename==NULL) {
    stream = stdin;
  }
  else {
    stream = fopen(filename, "r");
    if (stream == NULL) {
      perror(filename);
      exit(1);
    }
  }
  
  md5_init(&md5);
  
  /* iterate over file */
  while (1) {
    
    /* read in from our file */
    ret = fread(buffer, sizeof(char), sizeof(buffer), stream);
    if (ret <= 0)
      break;
    
    /* process our buffer buffer */
    md5_process(&md5, buffer, ret);
  }
  
  md5_finish(&md5, sig); 
  
  if (stream != stdin) {
    (void)fclose(stream);
  }
  
  if (DEBUG2) {
    printf("\n      Work:printing sig=");
    print_sig(sig);
  }

  unit_result_t result = (unit_result_t)malloc(sizeof(char)*(RESULT_MAX_SIZE)); 
  memset(result,'\0',RESULT_MAX_SIZE);
  memcpy(result,sig,MD5_SIZE);

  if (filename==NULL) {
    strcat(result+MD5_SIZE,"\0");
  } 
  else {
    int i;
    for (i=0;i<NUM_BLANK_SPACES;i++) {
        result[MD5_SIZE + i] = ' ';
    }
    result[MD5_SIZE + NUM_BLANK_SPACES + 1] = '\0';
    strcat(result+MD5_SIZE,filename);
    strcat(result,"\0");
  }

  if (DEBUG2) {
      printf("\n      Work:full result=");
      print_sig(sig);
      printf("%s",result+MD5_SIZE);
  }

  return result;
}


/*
 * Read file list from stdin/file and run MD5 for each file
 */
static	void	read_list(const char *filename, unit_of_work_t *list_of_work, int *ntasks)
{
  char		*buffer;
  char		*ret;
  char          *nl;
  FILE		*stream;
  
  if (filename==NULL) {
    stream = stdin;
  }
  else {
    stream = fopen(filename, "r");
    if (stream == NULL) {
      perror(filename);
      exit(1);
    }
  }

  /* iterate over file */
  while (1) {
    
    buffer = (char *)malloc(sizeof(char)*FILENAME_MAX);
    memset(buffer,'0',FILENAME_MAX);

    /* read in from our file */
    ret = fgets(buffer, FILENAME_MAX, stream);
    if (ret == NULL)
      break;

    /* remove \n from buffer */    
    nl = strrchr(buffer, '\n');
    if (nl) *nl = '\0';

    if (DEBUG2) {
       printf("\n     (reading task list) work=%s ntasks=%d\n",buffer,*ntasks);
    }

    list_of_work[*ntasks]=buffer;
    (*ntasks)++;

  }
}


/*
 * Get next work item
 */
static unit_of_work_t get_work_item(unit_of_work_t *list_of_work, int task)
{
  /* Obtain a new unit of work suitable to be given to a slave. */
  return list_of_work[task];
}


/*
 * Result output (printing stdout)
 */
static void 	process_results(unit_result_t result)
{
  /* Print the results returned by the slave */
  print_sig(result);
  printf("%s",(char *)&(result[MD5_SIZE]));
  printf("\n");
}


/*
 * Do work and return the result
 */
static unit_result_t do_work(unit_of_work_t work)
{
  /* Process the work and generate a result */
  return read_file(work);
}


/*
 * Add job to the finished list
 */
static void addFinishedJob(unit_result_t job,int pos)
{
  int disp = pos * (MD5_SIZE+1);
  memcpy(listFinishedJobs+disp,job,MD5_SIZE+1);
}


static void inicializeGobalVars()
{
    int *max_tag;
    int flag;
    MPI_Attr_get(MPI_COMM_WORLD, MPI_TAG_UB, (void*)&max_tag, &flag);
    if(flag){
      if(max_num_tasks>=(*max_tag)){
        max_num_tasks = (*max_tag)-2;
        dieTag = max_num_tasks-1;
      }
    }

    listFinishedJobs = malloc(sizeof(char *)*MD5_SIZE*max_num_tasks);
}








