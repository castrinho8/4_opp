#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mpi.h>
#include <assert.h>
#include "md5.h"

#define ROOT 0
#define MAX_NUM_TASKS 35536
#define NUM_BLANK_SPACES 2
#define RESULT_MAX_SIZE MD5_SIZE+NUM_BLANK_SPACES+FILENAME_MAX
#define CALC_IT_PER_PROC(N,P,R) N/P + (R < N % P)

#ifndef DEBUG
  #define DEBUG  0
#endif 

#ifndef DEBUG2
  #define DEBUG2 0
#endif 

#ifndef PRINT_RESULT
  #define PRINT_RESULT  1
#endif 

/* Local functions data work*/
static int getMD5FromListOfWork(char * * result_list,char * list_of_work,int ntasks);
static char * do_work(char * work);
static void print_results (char * work_list, char * result_list, int * sendcounts,int first_proc, int last_proc);
static void process_results(char * result);

/* Local functions md5sumlist*/
static void usage(void);
static void print_sig(const unsigned char *sig);
static char *read_file(const char *filename);
static void read_list(const char *filename, char * list_of_work, int *ini_list, int *ntasks);

int max_num_tasks = MAX_NUM_TASKS;
int dieTag = MAX_NUM_TASKS+1;

/*
 * Main
 */
int main(int argc, char **argv)
{

	//Compartidas
	int myrank,numprocs;
	double maxtime,mintime,avgtime,mytime;
	int error;
	int ntasks = 0;
	int recvcount;
	int * sendcounts, *senddispls, *num_elements;

	//Root
	char *infile = NULL;
	char * list_of_work;
	char * result_list;
	int i = 0;
	int sum_sendcounts = 0;
	int * ini_list;
 	int * sendcounts_result, *displs_result;

	//Hijos
	char * local_work_list;
	char * local_result_list;
 	int length_result_list = 0;

 	//Inicio
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

	//Comprobamos los argumentos
	if ((numprocs < 1)||(argc != 2)) {
	  if (myrank == ROOT)
		 usage();

	  MPI_Finalize();
	  return -1;
	}
	
	//Sincroniza procesos
	MPI_Barrier(MPI_COMM_WORLD);
	//Obtiene el tiempo
	mytime = MPI_Wtime();

	//Inicializamos variables
  	if (myrank == ROOT) {
		//Nombre de archivo donde está la lista de archivos a analizar
   		infile = argv[1];

		//Reservamos memoria para listas de trabajos
		list_of_work = malloc(sizeof(char)*FILENAME_MAX*max_num_tasks);
		assert(list_of_work);
		
		//Reserva memoria para la lista que indica la posición de cada trabajo en el array
		ini_list = malloc(sizeof(int)*max_num_tasks);
		assert(ini_list);

		//Leemos la lista de archivos del fichero proporcionado
		read_list(infile, list_of_work, ini_list, &ntasks);
		
		//Reservamos memoria para listas de resultados
		result_list = malloc(sizeof(char)*(MD5_SIZE)*ntasks);
		assert(result_list);

		if(DEBUG2){
			printf("\n\t[PROCESS #%d ] List of work:\n",myrank);
			int l = 0;
			for(l=0;l<=ntasks;l++){
				printf("\t(%d)-%s-\n",ini_list[l],list_of_work+ini_list[l]);
			}
		}
   }

  	//Para que todos los procesos conozcan el número de tareas a procesar
	error = MPI_Bcast(&ntasks,
						1,
						MPI_INT,
						ROOT, 
		      			MPI_COMM_WORLD);

  	if(error != MPI_SUCCESS){
  		if(myrank==ROOT)
	  		printf("Error(%d) en el MPI_Bcast\n",error);

	  	MPI_Finalize();
  		return -1;
  	}


   // Fase 1: Comunicación
   // Scatter del array con los nombres de ficheros a ordenar

	//Inicializamos variables Scatter envio
	sendcounts = malloc(sizeof(int) * numprocs);
	assert(sendcounts);

	num_elements = malloc(sizeof(int) * numprocs);
	assert(num_elements);

	senddispls = malloc(sizeof(int) * numprocs);
	assert(senddispls);

	//Reservamos espacio para las variables de recepcion
	sendcounts_result = malloc(sizeof(int)*numprocs);
	assert(sendcounts_result);

	displs_result = malloc(sizeof(int)*numprocs);
	assert(displs_result);

  	if(myrank==ROOT){
	   for(i=0; i < numprocs; i++){

	   		//ENVIO TRABAJOS:
	 		//Calcula el numero de ficheros a enviar a cada proceso
	 		sendcounts[i] = CALC_IT_PER_PROC(ntasks, numprocs, i);

	 		//Calcula los desplazamientos a partir de la lista de posiciones iniciales y send_counts
			senddispls[i] = i ? ini_list[sum_sendcounts] : 0;
			sum_sendcounts += sendcounts[i];

			//Calcula el numero de elementos (CHAR) que recibe cada proceso.
	 		num_elements[i] = ini_list[sum_sendcounts]-senddispls[i];

 	 		if(DEBUG){
 	 			printf("\n\t[PROCESS #%d ] Parametros Scatterv:\n",myrank);
 				printf("\tini_list[%d] =\t%d\n",sum_sendcounts-sendcounts[i],ini_list[sum_sendcounts-sendcounts[i]]);
	 			printf("\tsendcounts[%d] =\t%d\n",i,sendcounts[i]);
	 			printf("\tsenddispls[%d] =\t%d\n",i,senddispls[i]);
				printf("\tNum_elements[%d]=%d\n",i,num_elements[i]);	
			}

			//RECEPCIÓN RESULTADOS:
			//Calcula el numero de elementos a recibir
			sendcounts_result[i] = sendcounts[i]*(MD5_SIZE);

			//El desplazamiento es el MD5 +1 porque entre cada MD5 hay un espacio
			displs_result[i] = i ? (displs_result[i-1]+sendcounts_result[i-1]) : 0;

			if(DEBUG){
				printf("\n\t[PROCESS #%d ] Parametros Gatherv:\n",myrank);
				printf("\n\t[PROCESS #%d ] DISPLS_RESULT:%d\n",i,displs_result[i]);
				printf("\n\n\t[PROCESS #%d ] SENDCOUNT_RESULT:%d",i,sendcounts_result[i]);
			}
		}
	  	free(ini_list);
  	}

  	//Enviamos los datos necesarios a los diversos procesos
	MPI_Bcast(sendcounts,numprocs,MPI_INT,ROOT,MPI_COMM_WORLD);
	MPI_Bcast(senddispls,numprocs,MPI_INT,ROOT,MPI_COMM_WORLD);
	MPI_Bcast(num_elements,numprocs,MPI_INT,ROOT,MPI_COMM_WORLD);

  	//Inicializamos variables Scatter recepcion
	recvcount = num_elements[myrank];

	//Lista que contiene los ficheros en los que realizar el MD5
  	local_work_list = malloc(sizeof(char) * recvcount * FILENAME_MAX);
   	assert(local_work_list);

  	if(DEBUG){
  		printf("\n\t[PROCESS #%d ]- Enviando MPI_Scatterv con tamaño %d",myrank,recvcount);
  	}

  	//Enviamos la lista de trabajos
 	error = MPI_Scatterv(list_of_work,
		       				num_elements,
		                  	senddispls,
		                  	MPI_CHAR,
		                  	local_work_list,
		                  	recvcount,
		                  	MPI_CHAR,
		                  	ROOT,
		                  	MPI_COMM_WORLD);

  	if(error != MPI_SUCCESS){
  		if(myrank==ROOT)
	  		printf("Error(%d) en el MPI_Scatterv Thread:%d\n",error,myrank);

	  	MPI_Finalize();
  		return -1;
  	}

	free(num_elements);
	free(senddispls);

	// Fase 2: Computo
	// Procesamiento local en cada proceso MPI de local_work_list

  	//Reservamos memoria para la lista local de resultados
	local_result_list = malloc(sizeof(char)*MD5_SIZE*sendcounts[myrank]);
	assert(local_result_list);
	
	memset(local_result_list,'\0',sizeof(char)*MD5_SIZE*sendcounts[myrank]);


	//Calculamos el MD5 para la lista local de trabajos
	length_result_list = getMD5FromListOfWork(&local_result_list,local_work_list,sendcounts[myrank]);


	// Fase 3: Comunicación
  	//Recolección de resultados con MPI_Gather y ordenacion e impresión de los mismos.
	error = MPI_Gatherv(local_result_list,
							length_result_list,
							MPI_CHAR,
							result_list,
							sendcounts_result, 
							displs_result,
							MPI_CHAR,
							ROOT,
							MPI_COMM_WORLD);

  	if(error != MPI_SUCCESS){
  		if(myrank==ROOT)
	  		printf("Error(%d) en el Gather\n",error);

	  	MPI_Finalize();
  		return -1;
  	}

  	//Obtiene el tiempo final
	mytime = MPI_Wtime() - mytime;


  	if(DEBUG)
	  	printf("\n\t[PROCESS #%d ] Obtiene tiempos:",myrank);

	//compute max, min, and average timing statistics
	MPI_Reduce(&mytime, &maxtime, 1, MPI_DOUBLE, MPI_MAX, ROOT, MPI_COMM_WORLD);
	MPI_Reduce(&mytime, &mintime, 1, MPI_DOUBLE, MPI_MIN, ROOT, MPI_COMM_WORLD);
	MPI_Reduce(&mytime, &avgtime, 1, MPI_DOUBLE, MPI_SUM, ROOT, MPI_COMM_WORLD);
  
  	//Impresión de resultados
  	if (myrank == ROOT) {

		if(PRINT_RESULT){
			printf("\n-------------------------------------------------\n");
			print_results(list_of_work,result_list,sendcounts,0,numprocs);
			printf("\n-------------------------------------------------\n");
		}

		//Imprime tiempos
		avgtime /= numprocs;
	  	printf("Min: %lf Max: %lf Avg: %lf\n", mintime, maxtime,avgtime);
	  	
	  	free(list_of_work);
	  	free(result_list);
	}
	


	free(sendcounts);

	free(sendcounts_result);
  	free(displs_result);

	free(local_work_list);
	free(local_result_list);

	MPI_Finalize();
	return 0;
}



static int getMD5FromListOfWork(char ** result_list,char * list_of_work,int ntasks)
{
  	char * work;
  	char * result;

  	int myrank,i;
  	int disp_work = 0;
  	int disp_result = 0;

 	work = malloc (sizeof(char)*FILENAME_MAX);
  	result = malloc (sizeof(char)*RESULT_MAX_SIZE);

  	MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

  	for(i=0;i<ntasks;i++){

		memset(work,'\0',FILENAME_MAX);
		memset(result,'\0',RESULT_MAX_SIZE);

	 	//Obtenemos el elemento a procesar
	 	sprintf(work,"%s",list_of_work+disp_work);

	 	if (DEBUG){
			printf("\n\t[PROCESS #%d receiving] work=%s length=%d\n",myrank,work,strlen(work));
	 	}

	 	//Do the work
	 	result = do_work(work);

	 	if (DEBUG) {
		 	printf("\n\t[PROCESS #%d] result:",myrank);
		 	process_results(result);
	 	}

	 	//Añadimos el resultado a la lista
	 	memcpy(*result_list+disp_result,result,MD5_SIZE);

	 	//Calcula el desplazamiento en el trabajo de entrada y en el resultado
	 	disp_work += strlen(work)+1;
	 	disp_result += MD5_SIZE;
  	}
  	free(work);
  	free(result);

  	return disp_result;
}


/*
 * Read in from stdin/file and run MD5 on the input
 */
static	char 	*read_file(const char *filename)
{
  unsigned char	sig[MD5_SIZE];
  char		buffer[4096];
  md5_t		md5;
  int			ret;
  FILE		*stream;
	

  if (filename==NULL) {
	 stream = stdin;
  }
  else {
	 stream = fopen(filename, "r");
	 if (stream == NULL) {
		perror(filename);
		exit(1);
	 }
  }
  
  md5_init(&md5);
  
  /* iterate over file */
  while (1) {
	 
	 /* read in from our file */
	 ret = fread(buffer, sizeof(char), sizeof(buffer), stream);
	 if (ret <= 0)
		break;
	 
	 /* process our buffer buffer */
	 md5_process(&md5, buffer, ret);
  }

  md5_finish(&md5, sig); 
  
  if (stream != stdin) {
	 (void)fclose(stream);
  }
  
  if (DEBUG2) {
	 printf("\n      Work:printing sig=");
	 print_sig(sig);
  }

  char * result = (char *)malloc(sizeof(char)*(RESULT_MAX_SIZE)); 
  memset(result,'\0',RESULT_MAX_SIZE);
  memcpy(result,sig,MD5_SIZE);

  if (filename==NULL) {
	 strcat(result+MD5_SIZE,"\0");
  } 
  else {
	 int i;
	 for (i=0;i<NUM_BLANK_SPACES;i++) {
		  result[MD5_SIZE + i] = ' ';
	 }
	 result[MD5_SIZE + NUM_BLANK_SPACES + 1] = '\0';
	 strcat(result+MD5_SIZE,filename);
	 strcat(result,"\0");
  }

  if (DEBUG2) {
		printf("\n      Work:full result=");
		print_sig(sig);
		printf("%s",result+MD5_SIZE);
  }

  return result;
}


/*
 * Read file list from stdin/file and run MD5 for each file
 */
static	void	read_list(const char *filename, char * list_of_work, int *ini_list, int *ntasks)
{
	char		*ret;
	char          *nl;
	FILE		*stream;
	int i = 0,desplazamiento = 0;

	if (filename==NULL) {
		stream = stdin;
	}
	else {
	 	stream = fopen(filename, "r");
	 	if (stream == NULL) {
			perror(filename);
			exit(1);
	 	}	
	}

	/* iterate over file */
	while (i<max_num_tasks) {
	 
		/* read in from our file */
		ret = fgets(list_of_work+desplazamiento, FILENAME_MAX, stream);
		if (ret == NULL)
			break;

		/* remove \n from buffer */    
		nl = strrchr(list_of_work+desplazamiento, '\n');
		if (nl) *nl = '\0';

		if (DEBUG2) {
			printf("\n     (reading task list) work=%s ntasks=%d\n",list_of_work+desplazamiento,*ntasks);
		}

		ini_list[i] = desplazamiento;
		desplazamiento += strlen(list_of_work+desplazamiento)+1;
		(*ntasks)++;
		i++;
	}
	ini_list[i]=desplazamiento;
}



/*
 * Imprime los resultados de la lista, para los procesos 
 * exitentes entre first_proc y last_proc
 */
static void print_results(char * work_list, char * result_list, int * sendcounts,int first_proc, int last_proc)
{
	int y=0,leng=0,proc=0,work_disp=0;
	for(proc=first_proc;proc<last_proc;proc++){
		//Bucle que imprime los resultados de cada proceso
		while(y<sendcounts[proc]){
 			print_sig(result_list+leng);
			printf("  %s\n",work_list+work_disp);
	  		leng += MD5_SIZE;
			y++;
			work_disp += strlen(work_list+work_disp)+1;
		}
		y=0;
	}
}


/*
 * Print an md5 signature
 */
static	void	print_sig(const unsigned char *sig)
{
  const unsigned char	*sig_p;
  
  for (sig_p = sig; sig_p < sig + MD5_SIZE; sig_p++) {
	 printf("%02x", *sig_p);
  }
}


/*
 * Result output (printing stdout)
 */
static void 	process_results(char * result)
{
  /* Print the results returned by the slave */
  print_sig(result);
  printf("%s",(char *)&(result[MD5_SIZE]));
  printf("\n");
}


/*
 * Do work and return the result
 */
static char * do_work(char * work)
{
  /* Process the work and generate a result */
  return read_file(work);
}


/*
 * print the usage message
 */
static	void	usage(void)
{
  fprintf(stderr, " Usage: mpirun -n $NP md5sumlist [filelist]\n");
  fprintf(stderr, "        (where $NP > 1)\n");    
}




