############runmpi.sh################
#!/bin/bash

module load mpich2

#Crea el filelist del tamaño que se quiera
rm -rf filelist temp
> temp
find /var / -type f -perm -444 -xdev -fprint temp 
head -n 10000 temp > filelist
rm temp


#Ejecuta la práctica secuencial
time sh md5sumlist-script.sh filelist > seq.output

#Ejecuta la práctica con MPI
#mpirun valgrind --leak-check=full --show-reachable=yes ./md5sumlist filelist > mpi.output
mpirun ./md5sumlist filelist > mpi.output


