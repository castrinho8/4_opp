#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mpi.h>
#include <assert.h>
#include "md5.h"

#define ROOT 0
#define MAX_NUM_TASKS 65536
#define NUM_BLANK_SPACES 2
#define RESULT_MAX_SIZE MD5_SIZE+NUM_BLANK_SPACES+FILENAME_MAX

#ifndef DEBUG
  #define DEBUG  1
#endif 

#ifndef DEBUG2
  #define DEBUG2 0
#endif 

typedef char *unit_result_t;
typedef char *unit_of_work_t;

/* Local functions master/slave*/
static unit_of_work_t get_work_item( unit_of_work_t *list_of_work, int submittedtask);
static void process_results(unit_result_t result);
static unit_result_t do_work(unit_of_work_t work);

static unit_result_t * getMD5FromListOfWork(unit_of_work_t * list_of_work,int size_list);
static void addFinishedJob(unit_result_t job,int pos);
static void inicializeGobalVars();
static void printResults(unit_result_t * result_list,int size);

/* Local functions md5sumlist*/
static void usage(void);
static void print_sig(const unsigned char *sig);
static char *read_file(const char *filename);
static void read_list(const char *filename, unit_of_work_t *list_of_work, int *ntasks);

int max_num_tasks = MAX_NUM_TASKS;
int dieTag = MAX_NUM_TASKS+1;
unit_result_t * listFinishedJobs;

/*
 * Main
 */
int main(int argc, char **argv)
{

	//Compartidas
	int myrank,numprocs,maxtime,mintime,avgtime,mytime;
	int error;
	int ntasks = 0;
	//Root
	char *infile = NULL;
	unit_of_work_t * list_of_work;
	unit_result_t * result_list;
	//Hijos
	unit_of_work_t * local_work_list;
	unit_result_t * local_result_list;

	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

  	MPI_Errhandler_set(MPI_COMM_WORLD,MPI_ERRORS_RETURN);

	//Comprobamos los argumentos
	if ((numprocs < 2)||(argc != 2)) {
	  if (myrank == ROOT)
		 usage();

	  MPI_Finalize();
	  return -1;
	}
	
	MPI_Barrier(MPI_COMM_WORLD); /*synchronize all processes*/
	mytime = MPI_Wtime(); /*get time just before work section */

	//Inicializamos variables
  	if (myrank == ROOT) {
		inicializeGobalVars();

 		if (argc == 2) {
   		infile = argv[1];
		}

  	  	//result = (unit_result_t) malloc (sizeof(char)*RESULT_MAX_SIZE);
	  	//list_of_work = (unit_of_work_t *) malloc(sizeof(unit_of_work_t *)*max_num_tasks);
	  	//result_list = (unit_result_t *) malloc(sizeof(unit_result_t *)*max_num_tasks);
		
		list_of_work = malloc(sizeof(char *)*max_num_tasks);
		result_list = malloc(sizeof(char *)*max_num_tasks);
		read_list(infile, list_of_work, &ntasks);
   }

  	//Comprobamos si ntasks divide a numprocs, sino imprimimos error.
  	if((ntasks % numprocs) != 0){
	  if (myrank == ROOT)
	  		printf("El numero de tareas: %d no divide al numero de procesos: %d\n",ntasks,numprocs);

	  MPI_Finalize();
	  return -1;
	}

  	//Para que todos los procesos conozcan el número de tareas a procesar
	error = MPI_Bcast(&ntasks,
								1,
								MPI_INT,
								ROOT, 
               			MPI_COMM_WORLD);

  	if(error != MPI_SUCCESS){
  		if(myrank==ROOT)
	  		printf("Error(%d) en el Bcast\n",error);

	  	MPI_Finalize();
  		return -1;
  	}

	//Reserva de espacio para ntasks/numprocs unit_of_work_t en los hijos
	printf("\t[PROCESS #%d ]- Reservando espacio:%d\n",myrank,ntasks/numprocs);
	local_work_list = malloc(sizeof(char *)*max_num_tasks);
	local_result_list = malloc(sizeof(char *)*max_num_tasks);

   // Fase 1: Comunicación
   // Scatter del array con los nombres de ficheros a ordenar
  	if(DEBUG){
  		printf("\t[PROCESS #%d ]- Enviando Scatter con tamaño %d\n",myrank,ntasks/numprocs);
  	}

  	//Realizar el scatter
  	error = MPI_Scatter(list_of_work, 											//Lista a enviar
							  	(ntasks/numprocs)*sizeof(char *),	//Tamaño lista a enviar
							  	MPI_CHAR,												//Tipo envio
							  	local_work_list, 										//Lista donde recibir
							  	(ntasks/numprocs)*sizeof(char *),	//Tamaño lista donde recibir
							  	MPI_CHAR,												//Tipo recepcion
							  	ROOT,														
								MPI_COMM_WORLD);

  	printf("MYRANk:%d-%d\n",myrank,max_num_tasks);

  	int i = 0;
 	for(i=0;i<max_num_tasks;i++){
		//printf("VEEEEEE %d-%d-\n",myrank,local_work_list[i]!=NULL);
 		if(local_work_list[i]!=NULL)
			printf("SALE SCATTER Rank:%d(%d)-%s-\n",myrank,i,local_work_list[i]);
	}

  	if(error != MPI_SUCCESS){
  		if(myrank==ROOT)
	  		printf("Error(%d) en el Scatter Proceso:%d\n",error,myrank);

	  	MPI_Finalize();
  		return -1;
  	}
/*
  	printf("ENTRA MD5\n");
   // Fase 2: Computo
   // Procesamiento local en cada proceso MPI
   //Procesamiento de local_work_list
	local_result_list = getMD5FromListOfWork(local_work_list,(ntasks/numprocs));
	printf("SALE MD5\n");

   // Fase 3: Comunicación
  	//Recolección de resultados con MPI_Gather y ordenacion e impresión de los mismos.

	if(DEBUG){
		printf("Recibiendo Gather con tamaño %d",ntasks/numprocs);
	}

  	error = MPI_Gather(local_result_list,										//Lista a enviar
  								(ntasks/numprocs)*(sizeof(unit_result_t *)),	//Tamaño a enviar
  								MPI_CHAR,												//Tipo de la lista a enviar
  								result_list,											//Lista donde recibir
  								(ntasks/numprocs)*(sizeof(unit_result_t *)),	//Tamaño de la lista donde recibir
  								MPI_CHAR,												//Tipo de la lista donde recibir
  								ROOT,
  								MPI_COMM_WORLD);

  	if(error != MPI_SUCCESS){
  		if(myrank==ROOT)
	  		printf("Error(%d) en el Gather\n",error);

	  	MPI_Finalize();
  		return -1;
  	}
*/
	mytime = MPI_Wtime() - mytime; /*get time just after work section*/

	/*compute max, min, and average timing statistics*/
	MPI_Reduce(&mytime, &maxtime, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&mytime, &mintime, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&mytime, &avgtime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  
  	//Impresión de resultados
  	if (myrank == ROOT) {
  		printResults(result_list,(ntasks/numprocs));
		avgtime /= numprocs;
	  	printf("Min: %lf Max: %lf Avg: %lf\n", mintime, maxtime,avgtime);
	}

	MPI_Finalize();
	return 0;
}



/*
 * print the usage message
 */
static	void	usage(void)
{
  fprintf(stderr, " Usage: mpirun -n $NP md5sumlist [filelist]\n");
  fprintf(stderr, "        (where $NP > 1)\n");    
}



static unit_result_t * getMD5FromListOfWork(unit_of_work_t * list_of_work,int size_list)
{
  unit_of_work_t work;
  unit_result_t result;
  unit_result_t * result_list;
  int myrank,i;

  //work = (unit_of_work_t) malloc (sizeof(char)*FILENAME_MAX);
  result_list = (unit_result_t *) malloc(sizeof(unit_result_t)*size_list);
  //result = (unit_result_t) malloc (sizeof(char)*RESULT_MAX_SIZE);

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

  	printf("PROCESO:%d\n",myrank);

  	for(i=0;i<size_list;i++)
  		printf("\nRank:%d(%d)-%s-\n",myrank,i,list_of_work[i]);

  	for(i=0;i<size_list;i++){
 		printf("EEE:%d - it:%d\n",myrank,i);

		//memset(work,'\0',FILENAME_MAX);

	 	//Obtenemos el elemento a procesar
	 	work = strdup(get_work_item(list_of_work,i));

	 	if (DEBUG) {
			printf("\n  [PROCESS #%d receiving] work=%s length=%d",myrank,work,strlen(work));
	 	}

	 	/* Do the work */
	 	result = strdup(do_work(work));

	 	if (DEBUG) {
		 	printf("\n  [PROCESS #%d processing] with length=%d Result=",myrank,strlen(result));
		 	process_results(result);
	 	}

	 	result_list[i] = result;
  	}
  	return result_list;
}

/*
 * Print an md5 signature
 */
static	void	print_sig(const unsigned char *sig)
{
  const unsigned char	*sig_p;
  
  for (sig_p = sig; sig_p < sig + MD5_SIZE; sig_p++) {
	 printf("%02x", *sig_p);
  }
}

/*
 * Read in from stdin/file and run MD5 on the input
 */
static	char 	*read_file(const char *filename)
{
  unsigned char	sig[MD5_SIZE];
  char		buffer[4096];
  md5_t		md5;
  int		ret;
  FILE		*stream;
  
  if (filename==NULL) {
	 stream = stdin;
  }
  else {
	 stream = fopen(filename, "r");
	 if (stream == NULL) {
		perror(filename);
		exit(1);
	 }
  }
  
  md5_init(&md5);
  
  /* iterate over file */
  while (1) {
	 
	 /* read in from our file */
	 ret = fread(buffer, sizeof(char), sizeof(buffer), stream);
	 if (ret <= 0)
		break;
	 
	 /* process our buffer buffer */
	 md5_process(&md5, buffer, ret);
  }
  
  md5_finish(&md5, sig); 
  
  if (stream != stdin) {
	 (void)fclose(stream);
  }
  
  if (DEBUG2) {
	 printf("\n      Work:printing sig=");
	 print_sig(sig);
  }

  unit_result_t result = (unit_result_t)malloc(sizeof(char)*(RESULT_MAX_SIZE)); 
  memset(result,'\0',RESULT_MAX_SIZE);
  memcpy(result,sig,MD5_SIZE);

  if (filename==NULL) {
	 strcat(result+MD5_SIZE,"\0");
  } 
  else {
	 int i;
	 for (i=0;i<NUM_BLANK_SPACES;i++) {
		  result[MD5_SIZE + i] = ' ';
	 }
	 result[MD5_SIZE + NUM_BLANK_SPACES + 1] = '\0';
	 strcat(result+MD5_SIZE,filename);
	 strcat(result,"\0");
  }

  if (DEBUG2) {
		printf("\n      Work:full result=");
		print_sig(sig);
		printf("%s",result+MD5_SIZE);
  }

  return result;
}


/*
 * Read file list from stdin/file and run MD5 for each file
 */
static	void	read_list(const char *filename, unit_of_work_t *list_of_work, int *ntasks)
{
  char		*buffer;
  char		*ret;
  char          *nl;
  FILE		*stream;
  
  if (filename==NULL) {
	 stream = stdin;
  }
  else {
	 stream = fopen(filename, "r");
	 if (stream == NULL) {
		perror(filename);
		exit(1);
	 }
  }

  /* iterate over file */
  while (1) {
	 
	 buffer = (char *)malloc(sizeof(char)*FILENAME_MAX);
	 memset(buffer,'0',FILENAME_MAX);

	 /* read in from our file */
	 ret = fgets(buffer, FILENAME_MAX, stream);
	 if (ret == NULL)
		break;

	 /* remove \n from buffer */    
	 nl = strrchr(buffer, '\n');
	 if (nl) *nl = '\0';

	 if (DEBUG2) {
		 printf("\n     (reading task list) work=%s ntasks=%d\n",buffer,*ntasks);
	 }

	 list_of_work[*ntasks]=buffer;
	 (*ntasks)++;

  }
}


/*
 * Get next work item
 */
static unit_of_work_t get_work_item(unit_of_work_t *list_of_work, int task)
{
	printf("WORK:%s\n",list_of_work[task]);
  /* Obtain a new unit of work suitable to be given to a slave. */
  return list_of_work[task];
}


/*
 * Result output (printing stdout)
 */
static void 	process_results(unit_result_t result)
{
  /* Print the results returned by the slave */
  print_sig(result);
  printf("%s",(char *)&(result[MD5_SIZE]));
  printf("\n");
}


/*
 * Do work and return the result
 */
static unit_result_t do_work(unit_of_work_t work)
{
  /* Process the work and generate a result */
  return read_file(work);
}


/*
 * Add job to the finished list
 */
static void addFinishedJob(unit_result_t job,int pos){

	unit_result_t trabajo = strdup(job);
	listFinishedJobs[pos] = trabajo;
}


static void inicializeGobalVars(){

	 int *max_tag;
	 int flag;
	 MPI_Attr_get(MPI_COMM_WORLD, MPI_TAG_UB, (void*)&max_tag, &flag);
	 if(flag){
		if(max_num_tasks>=(*max_tag)){
		  max_num_tasks = (*max_tag)-2;
		  dieTag = max_num_tasks-1;
		}
	 }

	 listFinishedJobs = malloc(sizeof(unit_result_t*)*max_num_tasks);
}


static void printResults(unit_result_t * result_list,int size){
	int i = 0;
	for(i=0;i<size;i++)
		process_results(result_list[i]);
}






