#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mpi.h>

#include "md5.h"

#define WORKTAG 1
#define DIETAG 2
#define MAX_NUM_TASKS 65536
#define NUM_BLANK_SPACES 2
#define RESULT_MAX_SIZE MD5_SIZE+NUM_BLANK_SPACES+FILENAME_MAX

#ifndef DEBUG
  #define DEBUG  0
#endif 

#ifndef DEBUG2
  #define DEBUG2 0
#endif 

typedef char *unit_result_t;
typedef char *unit_of_work_t;

/* Local functions master/slave*/
static void master(const char *filename);
static void slave(void);
static unit_of_work_t get_work_item( unit_of_work_t *list_of_work, int submittedtask);
static void process_results(unit_result_t result);
static unit_result_t do_work(unit_of_work_t work);

/* Local functions md5sumlist*/
static void usage(void);
static void print_sig(const unsigned char *sig);
static char *read_file(const char *filename);
static void read_list(const char *filename, unit_of_work_t *list_of_work, int *ntasks);

/*
 * Main
 */
int main(int argc, char **argv)
{
    int myrank, numprocs;
    char *infile = NULL;

    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

    if ((numprocs < 2)||(argc > 2)) {
      if (myrank ==0) {
	usage();
      }
      MPI_Finalize();
      return -1;
    }
    
    if (argc == 1) {
      infile==NULL;
    }
    else if (argc == 2) {
      infile = argv[1];
    }

    if (myrank == 0) {
       master(infile);
    } else {
       slave();
    }

    MPI_Finalize();
    return 0;
}

/*
 * print the usage message
 */
static	void	usage(void)
{
  fprintf(stderr, " Usage: mpirun -n $NP md5sumlist [filelist]\n");
  fprintf(stderr, "        (where $NP > 1)\n");    
}


/*
 * Master
 */
static void master(const char *filename)
{
  int numprocs, rank;
  unit_of_work_t work;
  unit_result_t result;
  unit_of_work_t *list_of_work;
  int ntasks = 0, submittedtasks = 0;
  MPI_Status status;

  result = (unit_result_t) malloc (sizeof(char)*RESULT_MAX_SIZE);

  list_of_work = (unit_of_work_t *)malloc(sizeof(unit_of_work_t *)*MAX_NUM_TASKS);
 
  read_list(filename, list_of_work, &ntasks);

  /* Find out how many processes there are in the default communicator */
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

  /* Seed the slaves; send one unit of work to each slave. */
  if (ntasks<numprocs) {
     numprocs = ntasks+1;
  }

  for (rank = 1; rank < numprocs; ++rank) {

    /* Find the next item of work to do */
    work = get_work_item(list_of_work, submittedtasks);
    submittedtasks++;

    if (DEBUG) {
       printf("\n  [MASTER sending] work=%s length=%d to SLAVE #%d \n",work,strlen(work), rank);
    }

    /* Send it to each rank */
    MPI_Send(work,             /* message buffer */
             strlen(work),     /* one data item */
             MPI_CHAR,         /* data item is an integer */
             rank,             /* destination process rank */
             WORKTAG,          /* user chosen message tag */
             MPI_COMM_WORLD);  /* default communicator */
  }

  /* Loop over getting new work requests until there is no more work to be done */

  work = get_work_item(list_of_work, submittedtasks);
  submittedtasks++;

  while (work != NULL) {

    /* Receive results from a slave */
    memset(result,'\0',RESULT_MAX_SIZE);
    MPI_Recv(result,           /* message buffer */
             RESULT_MAX_SIZE,  /* one data item */
             MPI_CHAR,         /* of type double real */
             MPI_ANY_SOURCE,   /* receive from any sender */
             MPI_ANY_TAG,      /* any type of message */
             MPI_COMM_WORLD,   /* default communicator */
             &status);         /* info about the received message */

    if (DEBUG) {
       int count;
       MPI_Get_count(&status, MPI_CHAR, &count);
       printf("\n  [MASTER collecting] result with length=%d and count=%d from SLAVE #%d\n",
		strlen(result), count, status.MPI_SOURCE);
    }

    process_results(result);

    if (DEBUG) {
       printf("\n  [MASTER sending] work=%s length=%d to SLAVE #%d\n", 
		work, strlen(work), status.MPI_SOURCE);
    }

    /* Send the slave a new work unit */
    MPI_Send(work,             /* message buffer */
             strlen(work),     /* one data item */
             MPI_CHAR,         /* data item is an integer */
             status.MPI_SOURCE,/* to who we just received from */
             WORKTAG,          /* user chosen message tag */
             MPI_COMM_WORLD);  /* default communicator */

    /* Get the next unit of work to be done */
    work = get_work_item(list_of_work, submittedtasks);
    submittedtasks++;

  }

  /* There's no more work to be done, so receive all the outstanding
     results from the slaves. */
  for (rank = 1; rank < numprocs; ++rank) {
    memset(result,'\0',RESULT_MAX_SIZE);
    MPI_Recv(result, RESULT_MAX_SIZE, MPI_CHAR, MPI_ANY_SOURCE,
             MPI_ANY_TAG, MPI_COMM_WORLD, &status);

    if (DEBUG) {
       int count;
       MPI_Get_count(&status, MPI_CHAR, &count);
       printf("\n  [MASTER collecting] result with length=%d and count=%d from SLAVE #%d\n",
		strlen(result), count, status.MPI_SOURCE);
    }

    process_results(result);
  }

  /* Tell all the slaves to exit by sending an empty message with DIETAG. */
  for (rank = 1; rank < numprocs; ++rank) {
    MPI_Send(0, 0, MPI_INT, rank, DIETAG, MPI_COMM_WORLD);
  }
}


/*
 * Slave
 */
static void slave(void)
{
  unit_of_work_t work;
  unit_result_t result;
  int myrank;
  MPI_Status status;

  work = (unit_of_work_t) malloc (sizeof(char)*FILENAME_MAX);
  result = (unit_result_t) malloc (sizeof(char)*RESULT_MAX_SIZE);

  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);

  while (1) {

    memset(work,'\0',FILENAME_MAX);

    /* Receive a message from the master */
    MPI_Recv(work, FILENAME_MAX, MPI_CHAR, 0, MPI_ANY_TAG,
             MPI_COMM_WORLD, &status);

    /* Check the tag of the received message. */
    if (status.MPI_TAG == DIETAG) {
      return;
    }

    if (DEBUG) {
       printf("\n  [SLAVE #%d receiving] work=%s length=%d",myrank,work,strlen(work));
    }

    /* Do the work */
    result = do_work(work);

    if (DEBUG) {
       printf("\n  [SLAVE #%d replaying] with result with length=%d",myrank,strlen(result));
       printf("\n  [SLAVE #%d replaying] Result=",myrank);
       process_results(result);
    }

    /* Send the result back */
    MPI_Send(result, MD5_SIZE+strlen(result+MD5_SIZE), MPI_CHAR, 0, 0, MPI_COMM_WORLD);
  }
}

/*
 * Print an md5 signature
 */
static	void	print_sig(const unsigned char *sig)
{
  const unsigned char	*sig_p;
  
  for (sig_p = sig; sig_p < sig + MD5_SIZE; sig_p++) {
    printf("%02x", *sig_p);
  }
}

/*
 * Read in from stdin/file and run MD5 on the input
 */
static	char 	*read_file(const char *filename)
{
  unsigned char	sig[MD5_SIZE];
  char		buffer[4096];
  md5_t		md5;
  int		ret;
  FILE		*stream;
  
  if (filename==NULL) {
    stream = stdin;
  }
  else {
    stream = fopen(filename, "r");
    if (stream == NULL) {
      perror(filename);
      exit(1);
    }
  }
  
  md5_init(&md5);
  
  /* iterate over file */
  while (1) {
    
    /* read in from our file */
    ret = fread(buffer, sizeof(char), sizeof(buffer), stream);
    if (ret <= 0)
      break;
    
    /* process our buffer buffer */
    md5_process(&md5, buffer, ret);
  }
  
  md5_finish(&md5, sig); 
  
  if (stream != stdin) {
    (void)fclose(stream);
  }
  
  if (DEBUG2) {
    printf("\n      Work:printing sig=");
    print_sig(sig);
  }

  unit_result_t result = (unit_result_t)malloc(sizeof(char)*(RESULT_MAX_SIZE)); 
  memset(result,'\0',RESULT_MAX_SIZE);
  memcpy(result,sig,MD5_SIZE);

  if (filename==NULL) {
    strcat(result+MD5_SIZE,"\0");
  } 
  else {
    int i;
    for (i=0;i<NUM_BLANK_SPACES;i++) {
        result[MD5_SIZE + i] = ' ';
    }
    result[MD5_SIZE + NUM_BLANK_SPACES + 1] = '\0';
    strcat(result+MD5_SIZE,filename);
    strcat(result,"\0");
  }

  if (DEBUG2) {
      printf("\n      Work:full result=");
      print_sig(sig);
      printf("%s",result+MD5_SIZE);
  }

  return result;
}


/*
 * Read file list from stdin/file and run MD5 for each file
 */
static	void	read_list(const char *filename, unit_of_work_t *list_of_work, int *ntasks)
{
  char		*buffer;
  char		*ret;
  char          *nl;
  FILE		*stream;
  
  if (filename==NULL) {
    stream = stdin;
  }
  else {
    stream = fopen(filename, "r");
    if (stream == NULL) {
      perror(filename);
      exit(1);
    }
  }

  /* iterate over file */
  while (1) {
    
    buffer = (char *)malloc(sizeof(char)*FILENAME_MAX);
    memset(buffer,'0',FILENAME_MAX);

    /* read in from our file */
    ret = fgets(buffer, FILENAME_MAX, stream);
    if (ret == NULL)
      break;

    /* remove \n from buffer */    
    nl = strrchr(buffer, '\n');
    if (nl) *nl = '\0';

    if (DEBUG2) {
       printf("\n     (reading task list) work=%s ntasks=%d\n",buffer,*ntasks);
    }

    list_of_work[*ntasks]=buffer;
    (*ntasks)++;

  }
}


/*
 * Get next work item
 */
static unit_of_work_t get_work_item(unit_of_work_t *list_of_work, int task)
{
  /* Obtain a new unit of work suitable to be given to a slave. */
  return list_of_work[task];
}


/*
 * Result output (printing stdout)
 */
static void 	process_results(unit_result_t result)
{
  /* Print the results returned by the slave */
  print_sig(result);
  printf("%s",(char *)&(result[MD5_SIZE]));
  printf("\n");
}


/*
 * Do work and return the result
 */
static unit_result_t do_work(unit_of_work_t work)
{
  /* Process the work and generate a result */
  return read_file(work);
}


