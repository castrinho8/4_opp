PROGRAM gauss
	IMPLICIT NONE

	INTEGER, PARAMETER :: double = SELECTED_REAL_KIND(P = 15)
	INTEGER, PARAMETER :: n = 2000 ! tamano matriz
	!REAL(KIND=double):: A(n,n), B(n), Y(n), X(n)
	REAL:: A(n,n), B(n), Y(n), X(n),factor(n),l1(n,n),l2(n)
	INTEGER i, j, ipv, ti, tf, hz
	REAL pivote

	! Determina el numero de tics por segundo
	CALL SYSTEM_CLOCK(COUNT_RATE = hz)

	! Inicializa A, x a valores aleatorios
	CALL RANDOM_NUMBER(A)
	CALL RANDOM_NUMBER(Y)

	B = MATMUL(A,Y)

	!PRINT *, " A es ",A
	!PRINT *, " Aint es ",Aint
	!PRINT *, " B es ",B
	!PRINT *, " Y es ",Y

	CALL SYSTEM_CLOCK(COUNT = ti)  ! Tiempo inicial
 
	DO ipv = 1,n
		IF (ABS(A(ipv,ipv)) .lt. 1.e-5) THEN
			PRINT *, "Error con el pivote"
			STOP
		ENDIF

		!Calculamos el pivote
		pivote = A(ipv, ipv)

		!Dividimos A Y B entre el pivote
		A(ipv,ipv:) = A(ipv,ipv:)/pivote
		B(ipv) = B(ipv)/pivote

		!Creamos los productos a restar de la matriz
		FORALL (j=1:n)
			l1(j,ipv:n) = A(i,ipv)*A(ipv,ipv:n)
		END FORALL
		l2(:) = A(:,ipv)*B(ipv)

		!Calculamos A y B para el bucle 1 : ipv-1
		A(1:ipv-1,ipv:n) = A(1:ipv-1,ipv:n)-l1(1:ipv-1,ipv:n)
		B(1:ipv-1)=B(1:ipv-1)-l2(1:ipv-1)

		!Calculamos A y B para el bucle ipv+1 : n
		A(ipv+1:n,ipv:n) = A(ipv+1:n,ipv:n)-l1(ipv+1:n,ipv:n)
		B(ipv+1:n)=B(ipv+1:n)-l2(ipv+1:n)
	ENDDO

	X = B
	
	CALL SYSTEM_CLOCK(COUNT = tf)  ! Tiempo final

	IF (n<100) THEN
	DO i=1,n
		IF (ABS(X(i)/Y(i)) .gt. (1.+(n/100.))) THEN
			PRINT *, "  *** ERROR en Resultado *** "
			PRINT *, " X es          ", X(i)
			PRINT *, " y deberia ser ", Y(i)
			PRINT *, " -la fila es i=", i
		ENDIF
	ENDDO
	ENDIF

	PRINT *," La practica tarda ", REAL (tf - ti)/REAL (hz), " segundos"
	!PRINT *," El numero de Procesadores es: ", NUMBER_OF_PROCESSORS()

END PROGRAM gauss
