PROGRAM gauss
  IMPLICIT NONE

  INTEGER, PARAMETER :: double = SELECTED_REAL_KIND(P = 15)
  INTEGER, PARAMETER :: n = 1500 ! tamano matriz
  !REAL(KIND=double):: A(n,n), B(n), Y(n), X(n)
  REAL:: A(n,n), B(n), Y(n), X(n)
  INTEGER i, j, ifila, icol, ipv, ti, tf, hz
  REAL pivote, factor

  ! Determina el numero de tics por segundo
  CALL SYSTEM_CLOCK(COUNT_RATE = hz)

  ! Inicializa A, x a valores aleatorios
  CALL RANDOM_NUMBER(A)
  CALL RANDOM_NUMBER(Y)

  B = MATMUL(A,Y)

  !PRINT *, " A es ",A
  !PRINT *, " Aint es ",Aint
  !PRINT *, " B es ",B
  !PRINT *, " Y es ",Y

  CALL SYSTEM_CLOCK(COUNT = ti)  ! Tiempo inicial
 
  DO ipv = 1,n
		IF (ABS(A(ipv,ipv)) .lt. 1.e-5) THEN
			PRINT *, "Error con el pivote"
			STOP
		ENDIF

		pivote = A(ipv, ipv)

		DO j=1,n
			A(ipv, j) = A(ipv, j)/pivote
		ENDDO

		B(ipv) = B(ipv)/pivote

		DO ifila = 1,n
			IF (ifila .ne. ipv) THEN
				factor=A(ifila,ipv)/A(ipv,ipv)
				DO icol = 1,n
					A(ifila,icol) = A(ifila,icol)-factor*A(ipv,icol)
				ENDDO
				B(ifila)=B(ifila)-factor*B(ipv)
			ENDIF
		ENDDO
	ENDDO

	DO i=1,n
		X(i) = B(i)
  	ENDDO
 
  CALL SYSTEM_CLOCK(COUNT = tf)  ! Tiempo final

  IF (n<100) THEN
  	DO i=1,n
		IF (ABS(X(i)/Y(i)) .gt. (1.+(n/100.))) THEN
			PRINT *, "  *** ERROR en Resultado *** "
		 	PRINT *, " X es          ", X(i)
		 	PRINT *, " y deberia ser ", Y(i)
			PRINT *, " -la fila es i=", i
		ENDIF
  	ENDDO
  ENDIF

  PRINT *," La practica tarda ", REAL (tf - ti)/REAL (hz), " segundos"
  !PRINT *," El numero de Procesadores es: ", NUMBER_OF_PROCESSORS()

END PROGRAM gauss
